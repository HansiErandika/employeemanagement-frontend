import { EmployeeView } from './../model/employeeView.model';
import { API_URL } from './../../environments/environment';
import { Observable } from 'rxjs';
import { Employee } from './../model/employee.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  refreshData(): Observable<EmployeeView[]>{
    return this.http.get<EmployeeView[]>(API_URL+'employee');
    
  }

  createEmployee(emp: Employee): Observable<Employee>{
    return this.http.post<Employee>(API_URL+'employee', emp);
    
  }

  updateEmployee(emp: Employee): Observable<Employee>{
    return this.http.put<Employee>(API_URL+'employee/'+ emp.employeeId, emp);
    
  }

  deleteEmployee(id: number){
    return this.http.delete(API_URL+'employee/'+ id);
    
  }

  saveFile(formData: any){
    return this.http.post<string>(API_URL+'employee/SaveFile', formData);
  }
}
