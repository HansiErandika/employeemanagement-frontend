import { Department } from './../model/department.model';
import { API_URL } from './../../environments/environment';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private http: HttpClient) { }

  refreshData(): Observable<Department[]>{
    return this.http.get<Department[]>(API_URL+'department');
    
  }

  createDepartment(dep: Department): Observable<Department>{
    return this.http.post<Department>(API_URL+'department', dep);
    
  }

  updateDepartment(dep: Department): Observable<Department>{
    return this.http.put<Department>(API_URL+'department/'+ dep.departmentId, dep);
    
  }

  deleteDepartment(id: number){
    return this.http.delete(API_URL+'department/'+ id);
    
  }
  // refreshData(): Observable<Department[]>{
  //   return this.http.get<Department[]>(API_URL+'department');
    
  // }
}
