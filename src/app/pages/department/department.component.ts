import { API_URL } from './../../../environments/environment';
import { Department } from './../../model/department.model';
import { DepartmentService } from './../../services/department.service';
import { Component, OnInit } from '@angular/core';
import axios from 'axios';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  department: Department[];
  modelTitle=''
  dep = new Department();

  constructor(private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.refreshdata();    

  }

  refreshdata(){
    this.departmentService.refreshData().subscribe((response: Department[]) => {
      this.department= response;
    });
  }

  addClick(){
    this.modelTitle="Add Department"
    this.dep.departmentId=0
    this.dep.departmentName=''
  }

  editClick(deprt: Department){
    this.modelTitle="Edit Department"
    this.dep.departmentId=deprt.departmentId
    this.dep.departmentName=deprt.departmentName
  }

  createClick(){
    this.departmentService.createDepartment(this.dep).subscribe((response)=>{
      this.refreshdata();
      alert("Succesfully added");
    });
  }
  
  updateClick(){
    this.departmentService.updateDepartment(this.dep).subscribe((response)=>{
      this.refreshdata();
      alert("Succesfully updated");
    });
  }

  deleteClick(id: number){
    if(!confirm("Are you sure?")){
      return
    }
    this.departmentService.deleteDepartment(id).subscribe((response)=>{
      this.refreshdata();
      alert("Succesfully deleted");
    });
  }

}
