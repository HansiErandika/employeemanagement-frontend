import { DomSanitizer } from '@angular/platform-browser';
import { PHOTO_URL, API_URL } from './../../../environments/environment';
import { DepartmentService } from './../../services/department.service';
import { Department } from './../../model/department.model';
import { EmployeeView } from './../../model/employeeView.model';
import { EmployeeService } from './../../services/employee.service';
import { Employee } from './../../model/employee.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employees: EmployeeView[];
  modelTitle = ''
  emp = new Employee();
  department: Department[];
  // image_path = PHOTO_URL;
  imagePath: string;
  constructor(private employeeService: EmployeeService, private departmentService: DepartmentService, private domSanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.refreshdata();
    this.getDeparment();

  }

  refreshdata() {
    this.employeeService.refreshData().subscribe((response: EmployeeView[]) => {
      this.employees = response;
    });
  }

  getDeparment() {
    this.departmentService.refreshData().subscribe((response: Department[]) => {
      this.department = response;
    });
  }
  addClick() {
    this.modelTitle = "Add Employee"
    this.emp.employeeId = 0
    this.emp.employeeName = ''
    this.emp.departmentId = 0
    this.emp.joinedDate = new Date();
    this.emp.joined = this.emp.joinedDate.toString().split('T')[0];
    this.emp.photoFileName = "D:/Photos/anonymus.png"
  }

  editClick(empl: EmployeeView) {
    this.modelTitle = "Edit Employee"
    this.emp.employeeId = empl.employeeId
    this.emp.employeeName = empl.employeeName
    this.emp.departmentId = empl.departmentId
    this.emp.joinedDate = empl.joinedDate
    this.emp.joined = this.emp.joinedDate.toString().split('T')[0];
    this.emp.photoFileName = empl.photoFileName

  }

  public uploadFile = (files: any) => {
    if (files.length === 0)
      return;

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    this.employeeService.saveFile(formData).subscribe((response) => {
      this.emp.photoFileName=response;
      // http://localhost:49960/api/Photos/girls_PNG6463.png
      this.refreshdata();
      console.log(response)
      // alert("Succesfully uploaded");
    });
  }

  imageUpload(event: any) {

    let formData = new FormData();

    formData.append('file', event.target.files[0]);
    console.log('event')
    this.employeeService.saveFile(formData).subscribe((response) => {
      this.refreshdata();
      console.log('event')
      alert("Succesfully uploaded");

    });
  }

  createClick() {
    this.emp.joinedDate = new Date(this.emp.joined)
    this.employeeService.createEmployee(this.emp).subscribe((response) => {
      this.refreshdata();
      alert("Succesfully added");
    });
  }

  updateClick() {
    // this.emp.joinedDate = new Date(this.emp.joined)
    // this.employeeService.updateEmployee(this.emp).subscribe((response) => {
    //   this.refreshdata();
    //   alert("Succesfully updated");
    // });
  }

  deleteClick(id: number) {
    if (!confirm("Are you sure?")) {
      return
    }
    this.employeeService.deleteEmployee(id).subscribe((response) => {
      this.refreshdata();
      alert("Succesfully deleted");
    });
  }

}
