export class EmployeeView {
    employeeId: number;
    employeeName: string;
    departmentName: string;
    departmentId: number;
    joinedDate:Date;
    joined:string;
    photoFileName:string="D:/Photos/anonymus.png";
}