export class Employee {
    employeeId: number;
    employeeName: string;
    departmentId: number;
    joinedDate:Date;
    joined:string;
    photoFileName:string="D:/Photos/anonymus.png";
}